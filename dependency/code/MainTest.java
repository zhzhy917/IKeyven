package dependency.code;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import _Keyven.UserDefine;

public class MainTest {
	public static void main(String[] args) {
		boolean choice = false;
		Scanner input = new Scanner(System.in);
		choice = input.nextBoolean();
		input.close();
		ArrayList<String> list = new ArrayList<>();
		UserDefine.addtemps();
		UserDefine.addwords();
		try {
			list = FileUtil.readFile("src/dependency/data/hankcs.txt", "gbk");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (String str : list) {
			System.out.print(str + "\t��\t");
			Dependency temp = new Dependency();
			DepObject obj = temp.process(str, choice);
			if (obj.isUseless()) {
				System.out.println(str);
			} else {
				System.out.println(obj.toString());
			}
		}
	}
}
